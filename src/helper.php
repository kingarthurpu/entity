<?php
/**
 * Created by PhpStorm
 * Slogan: Tomorrow's events will be known the day after tomorrow
 * Date: 2023/8/21 21:11
 * Desc: 自动注册指令，指令名称是 fm。
 *       你可以执行`php think`查看。
 *       auto register command, this command name is entity.
 *       you can run `php think` to view.
 */


use Kingarthur\Entity\FieldMapping;
use think\Console;
// 自动注册
// auto register command
Console::addDefaultCommands([
    'fm' => FieldMapping::class
]);