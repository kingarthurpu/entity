<?php
/**
 * Created by PhpStorm.
 * Slogan: Tomorrow's events will be known the day after tomorrow
 * Date: 2023/8/21
 * Time: 17:28
 * Desc: Utils class
 */

namespace Kingarthur\Entity;

class Utils
{
    /**
     * camelcase
     * @param string $str The string to be processed
     * @param bool $ucfirst Whether the first character is uppercase
     * @return mixed|string
     */
    static public function toCamelCase($str, $ucfirst = true)
    {
        if (!$str) return '';
        $array = explode('_', $str);
        if ($ucfirst) {
            $result = ucfirst($array[0]);
        } else {
            $result = $array[0];
        }
        $len = count($array);
        if ($len <= 1) {
            return $result;
        }
        for ($i = 1; $i < $len; $i++) {
            $result .= ucfirst($array[$i]);
        }
        return $result;
    }

    /**
     * 移除前缀
     * remove prefix
     * @param string $str 待处理字符串(The string to be processed)
     * @param string $prefix  要移除的前缀 （The prefix to remove）
     * @return false|mixed|string
     */
    static public function removePrefix($str, $prefix)
    {
        if (stripos($str, $prefix) === 0) {
            // 去除表前缀
            $str = substr($str, strlen($prefix) - 1);
        }

        return $str;
    }
}