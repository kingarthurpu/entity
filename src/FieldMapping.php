<?php
/**
 * Created by PhpStorm.
 * Slogan: Tomorrow's events will be known the day after tomorrow
 * Date: 2023/8/21
 * Time: 14:20
 * Desc: <p>生成数据表实体类.<br>就是单纯的记不住字段名称.</p>
 *
 * ----------------
 *  <p>Generate the data table entity class.<br>
 * I just can't remember the field name.</p>
 */

namespace Kingarthur\Entity;

use PDOStatement;
use think\Collection;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\Db;
use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use think\Exception;
use think\exception\DbException;
use think\facade\Config;
use think\facade\Env;

class FieldMapping extends Command
{
    protected $appPath = '';
    protected $path = '';

    protected $force = false;

    public function __construct($name = null)
    {
        parent::__construct($name);
        $this->appPath = Env::get('app_path');
        $this->path = $this->appPath . '/common/fieldmapping';
    }

    protected function configure()
    {
        $this->setName('fm')
            ->addArgument('tableName', Argument::OPTIONAL, "The name of the data table to create the entity class")
            ->addOption('force', null, Option::VALUE_NONE, 'Whether to force override')
            ->setDescription('Create a data table entity class');
    }

    protected function execute(Input $input, Output $output)
    {
        $tableName = trim($input->getArgument('tableName'));
        $this->force = $input->getOption('force');
        if (!$tableName) {
            // 创建所有数据表的实体类
            $this->createAllTable($output);
        } else {
            // 创建指定数据表的实体类
            $collection = $this->getTableDesc($tableName);
            $this->createEntity($collection, $tableName);
        }


        $output->writeln("The entity class has been created!");
    }

    /**
     * @return void
     * @throws DataNotFoundException
     * @throws DbException
     * @throws Exception
     * @throws ModelNotFoundException
     */
    protected function createAllTable(Output $output)
    {
        $collection = $this->getAllTableDesc();
        $array_column = array_column($collection, 'tableName');
        $tableList = array_unique($array_column);
        $tableListMap = [];
        foreach ($tableList as $tableName) {
            foreach ($collection as $key => $item) {
                if ($item['tableName'] !== $tableName) continue;
                if (isset($tableListMap[$tableName])) {
                    $tableListMap[$tableName][] = $item;
                } else {
                    $tableListMap[$tableName] = [
                        $item
                    ];
                }
                unset($collection[$key]);
            }
        }
        foreach ($tableList as $tableName) {
            if (false !== $this->createEntity($tableListMap[$tableName], $tableName)) {
                $output->writeln($tableName . ' entity class has been created!');
            }
        }
    }


    /**
     * 获取指定表的描述信息
     * Gets the description information for the specified table
     * @param $tableName
     * @return array|PDOStatement|string|Collection|\think\model\Collection
     * @throws DataNotFoundException
     * @throws ModelNotFoundException
     * @throws DbException
     */
    protected function getTableDesc($tableName)
    {
        $database = Config::get('database.database');
        $tableName = Config::get('database.prefix') . $tableName;
        $fields = [
            'COLUMN_NAME' => 'fieldName',
            'column_comment' => 'fieldComment',
            'column_type' => 'fieldType',
            'column_key' => 'constraints'
        ];
        return Db::table('information_schema.columns')
            ->field($fields)
            ->where('table_schema', $database)
            ->where('table_name', $tableName)
            ->select();
    }

    /**
     * 获取配置文件指定数据库的所有表的描述信息
     * Gets the description information for all tables in the specified database in the configuration file
     * @return array|PDOStatement|string|Collection|\think\model\Collection
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     * @throws Exception
     */
    protected function getAllTableDesc()
    {
        $database = Config::get('database.database');
        if (!$database) throw new Exception('Specify the database where you want to create the field mapping class!');
        $fields = [
            'TABLE_NAME' => 'tableName',
            'COLUMN_NAME' => 'fieldName',
            'column_comment' => 'fieldComment',
            'column_type' => 'fieldType',
            'column_key' => 'constraints'
        ];
        return Db::table('information_schema.columns')
            ->field($fields)
            ->order('TABLE_NAME', 'asc')
            ->where('table_schema', $database)
            ->select();
    }

    /**
     * 创建实体类
     * create entity class
     * @param Collection|array $collection query table info
     * @param string $tableName table name of entiry class
     * @return bool|int
     */
    protected function createEntity($collection, $tableName = '')
    {
        if (!file_exists($this->path) || !is_dir($this->path)) {
            mkdir($this->path, 0755, true);
        }
        return $this->createTraitFile($collection, $tableName);
    }

    /**
     * 获取实体类名称
     * entity name
     * @param string $tableName table name
     * @return string
     */
    protected function entityName($tableName)
    {
        return Utils::toCamelCase($tableName) . 'FieldMapping';
    }


    /**
     * 创建实体类trait文件
     * Create an entity class trait file
     * @param Collection|array $collection
     * @param string $tableName table name of entity class
     * @param $fileName
     * @return bool|int
     */
    protected function createTraitFile($collection, $tableName)
    {
        $tableName = Utils::removePrefix($tableName, Config::get('database.prefix'));

        $tableName = Utils::toCamelCase($tableName);
        $entity = $this->entityName($tableName);
        // 判断当前是否已经创建了实体类
        $filePath = $this->path . '/' . $entity . '.php';
        if (file_exists($filePath) && !$this->force) {
            return true;
        }
        $createDateTime = date('Y/m/d H:i');
        // 创建文件内容
        // create file content
        $head = <<<php
<?php
/**
 * Created by entity command
 * Date: {$createDateTime}
 * Desc: 
 */
namespace app\\common\\fieldmapping;
trait {$entity}
{
php;
        $staticProperty = '';

        $staticGetter = '';
        foreach ($collection as $item) {
            if(in_array($item['fieldName'], ['create_time','update_time','delete_time'])) continue;
            if(strtolower($item['constraints'])==='pri') continue;
            $propName = Utils::toCamelCase($item['fieldName'], false).'Field';
            $staticProperty .= PHP_EOL . <<<php
    protected static \${$propName} = '{$item['fieldName']}';
php;
            $functionName = Utils::toCamelCase($item['fieldName']).'Field';
            $staticGetter .= PHP_EOL . <<<php
    /**
     * {$item['fieldComment']}
     * @return string
     */
    public static function get{$functionName}()
    {
        return self::\${$propName};
    }
php;

        }

        $footer = <<<php
}
php;

        return file_put_contents($filePath, $head . PHP_EOL . $staticProperty . PHP_EOL . $staticGetter . PHP_EOL . $footer);
    }

}