# 数据表实体类生成

> 使用之前，需要完成database.php文件中的数据库配置。

## 项目介绍

生成数据表对应的字段映射类，提供get静态方法，映射数据表字段名称。

## 运行环境要求

- PHP >= 5.6.0
- topthink/framework 5.1.*

## 安装

```
composer require kingarthur/entity
```

安装完成后，系统将自动注册`fm`命令，可以使用`php think`查看。

## 实体类命名规则

- 生成的文件，默认存放在`application\common\fieldmapping`目录下。

- 文件的命名为 ： `不带前缀的表名+FieldMapping`的驼峰法形式。

## 实体类使用方式

生成的实体类是一个trait，只需要在对应的数据表模型中use即可。

trait中的get方法的注释来自定义数据表的comment。

大致结构如下：

```php
<?php
/**
 * Created by entity command
 * Date: 2023/08/22 10:37
 * Desc: 
 */
namespace app\common\fm;
trait LogsEntityFieldMapping
{

    protected static $zlUsername = 'zl_username';
    protected static $zlUrl = 'zl_url';


    /**
     * 用户名
     * @return string
     */
    public static function getZlUsernameField()
    {
        return self::$zlUsername;
    }
    /**
     * url
     * @return string
     */
    public static function getZlUrlField()
    {
        return self::$zlUrl;
    }
}
```

## 命令使用方式

### 指定数据表生成实体类


例如：在控制台输入`php think logs`，将会创建对应`logs`数据表的实体类。

**注意，输入的表名是不带表前缀的，默认会和`database.php`文件的中`prefix`项合并成完整的表名称。**

### 生成所有表对应的实体类

直接执行`php think fm`命令，将会生成所有数据表的实体类。

### 强制覆盖

默认情况下 ， 将会跳过已经生成了对应实体类的数据表，可以使用：

- `php think fm logs --force` 强制覆盖logs表生成的实体类；
- `php think fm --force` 强制重新生成所有数据表的实体类。
